from db_file_storage.model_utils import delete_file_if_needed, delete_file
from django.db import models

from django_cdstack_models.django_cdstack_models.models import CmdbHost


class CmdbHostImageData(models.Model):
    bytes = models.TextField()
    filename = models.CharField(max_length=255)
    mimetype = models.CharField(max_length=50)

    class Meta:
        db_table = "cmdb_host_image_data"


class CmdbHostImage(models.Model):
    host = models.ForeignKey(CmdbHost, models.CASCADE, null=True, blank=True)
    image = models.ImageField(
        upload_to="django_cdstack_doc_images.CmdbHostImageData/bytes/filename/mimetype",
        blank=True,
        null=True,
    )

    def save(self, *args, **kwargs):
        delete_file_if_needed(self, "image")
        super(CmdbHostImage, self).save(*args, **kwargs)

    def delete(self, *args, **kwargs):
        super(CmdbHostImage, self).delete(*args, **kwargs)
        delete_file(self, "image")

    class Meta:
        db_table = "cmdb_host_image"
